// Bài 2
tinhDiemTB = (...rest) => {
    let initialValue = 0;
    for(let i = 0; i < rest.length; i++){
       initialValue += rest[i];
    }
    return initialValue/rest.length;
}

tinhDiem1 = () => {;
    let diemToan = document.getElementById('inpToan').value*1;
    let diemLy = document.getElementById('inpLy').value*1;
    let diemHoa = document.getElementById('inpHoa').value*1;
    let finalCal1 = (tinhDiemTB(diemToan,diemLy,diemHoa)).toFixed(2);
    document.getElementById('tbKhoi1').innerHTML = finalCal1;
}

tinhDiem2 = () => {
    let diemVan = document.getElementById('inpVan').value*1;
    let diemSu = document.getElementById('inpSu').value*1;
    let diemDia = document.getElementById('inpDia').value*1;
    let diemEnglish = document.getElementById('inpEnglish').value*1;
    let finalCal2 = (tinhDiemTB(diemVan,diemSu,diemDia,diemEnglish)).toFixed(2);
    document.getElementById('tbKhoi2').innerHTML = finalCal2;
}

