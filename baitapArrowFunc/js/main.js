const colorList = ["pallet", "viridian", "pewter", "cerulean", "vermillion", "lavender","celadon","saffron","fuschia","cinnabar"];
let houseID = document.querySelector('.house');

showColor = () =>{
    let firstAction = colorList.map(item =>{
        return `<button onclick="changeColor('${item}')"class="color-button ${item}"></button>`
    });
    let secondAction = firstAction.join('');
    document.querySelector('#colorContainer').innerHTML = secondAction;
}

changeColor = (style)=> {
    let newClass = 'house' + ' ' + style;
    houseID.setAttribute("class", newClass);
}

window.onload = showColor()